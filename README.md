# Linux Nord Theme Wallpapers

## Getting started

I create wallpapers compatible only with Linux.

These are cute penguins and the color scheme is consistent with the "Nord" theme. (ref: https://www.nordtheme.com/)

They feature a **pixel perfect dithering** designed for the **1920 x 1200** screen resolution and compatible with the **1920 x 1080** resolution (if you use the correct zoom adjustment in your settings, to crop **only** the top and the bottom).

## Warning!

Please note that these wallpapers are **NOT** certified to be compatible with Windows or Mac. Use with these operating systems at your own risk!!!

